import { Types } from 'mongoose';
import { User } from 'src/users/schema/user.schema';

export interface MeasurementInterface {
  photoId: string;
  photoUrl: string;
  _id: Types.ObjectId;
  weight: number;
  chest: number;
  waist: number;
  hips: number;
  biceps: number;
  date: string;
  user: User;
}
