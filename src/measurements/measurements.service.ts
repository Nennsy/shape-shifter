import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, Types } from 'mongoose';
import { CreateMeasurementDto } from './dto/create-measurement.dto';
import { UpdateMeasurementDto } from './dto/update-measurement.dto';
import { MeasurementInterface } from './interfaces/measurement.interface';
import { Measurement, MeasurementDocument } from './schemas/measurement.schema';

@Injectable()
export class MeasurementsService {
  constructor(
    @InjectModel(Measurement.name)
    private measurementModel: Model<MeasurementDocument>,
  ) {}

  async getMeasurements(
    userId: Types.ObjectId,
  ): Promise<MeasurementDocument[]> | null {
    const measurements = await this.measurementModel.find({ user: userId });
    if (!measurements.length) return null;
    return measurements;
  }

  async getMeasurementById(
    id: string,
    userId: Types.ObjectId,
  ): Promise<MeasurementDocument> | null {
    return await this.measurementModel.findOne({ _id: id, user: userId });
  }

  async addMeasurement(
    measurement: CreateMeasurementDto,
    userId: Types.ObjectId,
  ): Promise<MeasurementInterface> {
    return this.measurementModel.create({
      ...measurement,
      user: userId,
    });
  }

  async updateMeasurement(
    id: string,
    updateMeasurementDto: UpdateMeasurementDto,
  ): Promise<MeasurementInterface> | null {
    const existingMeasurement = await this.measurementModel.findById({
      _id: id,
    });
    if (!existingMeasurement) {
      return null;
    } else {
      return await this.measurementModel.findByIdAndUpdate(
        id,
        updateMeasurementDto,
      );
    }
  }

  async deleteMeasurement(
    id: string,
    userId: Types.ObjectId,
  ): Promise<MeasurementInterface> | null {
    const existingMeasurement = await this.measurementModel.findOne({
      _id: id,
      user: userId,
    });
    if (!existingMeasurement) {
      return null;
    } else {
      await this.measurementModel.deleteOne({ _id: id, user: userId });
      return existingMeasurement;
    }
  }
}
