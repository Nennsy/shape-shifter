import { IsNumber, IsString } from 'class-validator';
export class CreateMeasurementDto {
  @IsString()
  photoUrl: string;
  @IsString()
  photoId: string;
  @IsNumber()
  weight: number;
  @IsNumber()
  chest: number;
  @IsNumber()
  waist: number;
  @IsNumber()
  hips: number;
  @IsNumber()
  biceps: number;
  @IsString()
  date: string;
}
