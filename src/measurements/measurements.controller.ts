import {
  Body,
  Controller,
  Delete,
  Get,
  HttpStatus,
  Param,
  Patch,
  Post,
  Req,
  Res,
  UseGuards,
} from '@nestjs/common';
import { JwtGuard } from 'src/auth/guards/jwt.guard';
import { CreateMeasurementDto } from './dto/create-measurement.dto';
import { UpdateMeasurementDto } from './dto/update-measurement.dto';
import { MeasurementInterface } from './interfaces/measurement.interface';
import { MeasurementsService } from './measurements.service';
import { MeasurementDocument } from './schemas/measurement.schema';

@Controller('measurements')
export class MeasurementsController {
  constructor(private readonly measurementService: MeasurementsService) {}

  @UseGuards(JwtGuard)
  @Get()
  async getAll(@Req() req, @Res() res): Promise<MeasurementDocument[]> {
    const measurements = await this.measurementService.getMeasurements(
      req.user.id,
    );
    if (!measurements) return res.status(HttpStatus.OK).json([]);
    res.status(HttpStatus.OK).json(measurements);
  }

  @UseGuards(JwtGuard)
  @Get(':id')
  async findById(
    @Param('id') measurementId: string,
    @Req() req,
  ): Promise<MeasurementDocument> | null {
    const userId = req.user.id;
    return await this.measurementService.getMeasurementById(
      measurementId,
      userId,
    );
  }

  @UseGuards(JwtGuard)
  @Post()
  addMeasurement(@Req() req, @Body() measurement: CreateMeasurementDto) {
    const userId = req.user.id;
    return this.measurementService.addMeasurement(measurement, userId);
  }
  @UseGuards(JwtGuard)
  @Patch(':id')
  async update(
    @Res() response,
    @Param('id') id: string,
    @Body() updateMeasurementDto: UpdateMeasurementDto,
  ): Promise<MeasurementInterface> {
    const updatedMeasurement = await this.measurementService.updateMeasurement(
      id,
      updateMeasurementDto,
    );
    if (!updatedMeasurement) {
      return response
        .status(HttpStatus.NOT_FOUND)
        .json({ message: 'There is no measurement with this id' });
    } else {
      return response.status(HttpStatus.OK).json({
        ...updatedMeasurement,
      });
    }
  }

  @UseGuards(JwtGuard)
  @Delete(':id')
  async delete(
    @Req() request,
    @Res() response,
    @Param('id') id: string,
  ): Promise<MeasurementInterface> {
    const userId = request.user.id;
    const measurement = await this.measurementService.deleteMeasurement(
      id,
      userId,
    );
    if (!measurement) {
      return response
        .status(HttpStatus.NOT_FOUND)
        .json({ message: 'There is no measurement with this id!' });
    } else {
      return response.status(HttpStatus.NO_CONTENT).json({});
    }
  }
}
