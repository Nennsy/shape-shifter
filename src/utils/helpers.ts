import * as bcrypt from 'bcrypt';

export function hashPassword(password: string): string {
  return bcrypt.hash(password, 12);
}
