import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import mongoose, { Document } from 'mongoose';
import { ExerciseType } from '../../exercise-type/schemas/exercise-type.schema';

export type ExerciseDocument = Exercise & Document;

@Schema()
export class Exercise {
  @Prop({
    required: true,
    type: mongoose.Schema.Types.ObjectId,
    ref: 'ExerciseType',
  })
  exerciseType: ExerciseType;
  @Prop({ required: true })
  series: number;
  @Prop()
  repetitions: number[];
  @Prop()
  weight: number;
  @Prop()
  time: number;
  @Prop()
  distance: number;
}

export const ExerciseSchema = SchemaFactory.createForClass(Exercise);