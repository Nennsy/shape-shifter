import {
    Controller,
    Get,
    Post,
    Body,
    Patch,
    Param,
    Delete,
    Res,
    HttpStatus
} from '@nestjs/common';
import { ExerciseService } from './exercise.service';
import { ExerciseInterface } from './interfaces/exercise.interface';
import { CreateExerciseDto } from './dto/create-exercise.dto';
import { UpdateExerciseDto } from './dto/update-exercise.dto';

@Controller('exercise')
export class ExerciseController {

constructor(private readonly exercisesService: ExerciseService) { }

    @Post()
    async create(
        @Body() createExerciseDto: CreateExerciseDto,
    ): Promise<ExerciseInterface> {
        return this.exercisesService.createExercise(createExerciseDto);
    }

    @Get()
    async findAll(): Promise<ExerciseInterface[]> {
      return this.exercisesService.findAllExercises();
    }

    
  @Get(':id')
  async findOne(@Res() response, @Param('id') id: string) {
    const exercise = await this.exercisesService.findExerciseById(id);
    if (!exercise) {
      return response
        .status(HttpStatus.NOT_FOUND)
        .json({ message: 'Exercise with this id does not exist' });
    } else {
      return response.status(HttpStatus.OK).json({ exercise });
    }
  }

  @Delete(':id')
  async remove(@Res() response, @Param('id') id: string): Promise<any> {
    const exercise = await this.exercisesService.deleteExercise(id);
    if (!exercise) {
      return response
        .status(HttpStatus.NOT_FOUND)
        .json({ message: 'Exercise with this id does not exist' });
    } else {
      return response.status(HttpStatus.NO_CONTENT).json({});
    }
  }

  @Patch(':id')
  async update(
    @Res() response,
    @Param('id') id: string,
    @Body() updateExerciseDto: UpdateExerciseDto,
  ): Promise<ExerciseInterface> | null {
    const exercise = await this.exercisesService.updateExercise(id, updateExerciseDto);
    if (!exercise) {
      return response
        .status(HttpStatus.NOT_FOUND)
        .json({ message: 'Exercise with this id does not exist' });
    } else {
      return response.status(HttpStatus.OK).json({ exercise });
    }
  }

}
