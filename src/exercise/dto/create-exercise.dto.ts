import { IsArray, IsNumber, IsOptional, IsString } from 'class-validator';

export class CreateExerciseDto {
  @IsString()
  exerciseType: string;
  @IsNumber()
  series: number;
  @IsOptional()
  @IsArray()
  repetitions: number[];
  @IsOptional()
  @IsNumber()
  weight: number;
  @IsOptional()
  @IsNumber()
  time: number;
  @IsOptional()
  @IsNumber()
  distance: number;
}
