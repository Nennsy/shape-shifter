import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { ExerciseInterface } from './interfaces/exercise.interface';
import { Exercise, ExerciseDocument } from './shemas/exercise.shema';
import { CreateExerciseDto } from './dto/create-exercise.dto';
import { UpdateExerciseDto } from './dto/update-exercise.dto';
@Injectable()
export class ExerciseService {

    constructor(
        @InjectModel(Exercise.name)
        private exerciseModel: Model<ExerciseDocument>,
      ) {}

    async createExercise(
        createExerciseDto: CreateExerciseDto,
      ): Promise<ExerciseInterface> {
        const exercise = new this.exerciseModel({ ...createExerciseDto });
        return exercise.save();
      }

      async findAllExercises(): Promise<ExerciseInterface[]> {
        return this.exerciseModel.find().lean();
      }

      async findExerciseById(id: string): Promise<ExerciseInterface> | null {
          const exercise = await this.exerciseModel.findById(id).lean();
          if (!exercise) {
            return null;
      }
    }
 
    async deleteExercise(id: string): Promise<ExerciseInterface> | null {
        const exercise = await this.findExerciseById(id);
        if (!exercise) {
          return null;
        } else {
          await this.exerciseModel.deleteOne({ _id: id });
          return exercise;
        }
      }

      updateExercise(
        id: string,
        updateExerciseDto: UpdateExerciseDto,
      ): Promise<ExerciseInterface> | null {
        return this.exerciseModel
          .findByIdAndUpdate(id, updateExerciseDto, {
            new: true,
          })
          .lean()
          .catch(() => {
            return null;
          });
      }

}
