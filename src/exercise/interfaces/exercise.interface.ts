import { ExerciseType } from "../../exercise-type/schemas/exercise-type.schema";

export interface ExerciseInterface {
  _id: string;
  exerciseType: ExerciseType;
  series: number;
  repetitions?: number[];
  weight?: number;
  time?: number;
  distance?: number;
}
