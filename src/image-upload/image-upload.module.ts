import { Module } from '@nestjs/common';
import { CloudinaryService } from 'src/cloudinary/cloudinary.service';
import { ImageUploadController } from './image-upload.controller';
import { ImageUploadService } from './image-upload.service';

@Module({
  imports: [],
  controllers: [ImageUploadController],
  providers: [ImageUploadService, CloudinaryService],
})
export class ImageUploadModule {}

