import {
  Controller,
  Post,
  Delete,
  UploadedFile,
  Param,
  BadRequestException,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { UploadApiErrorResponse, UploadApiResponse } from 'cloudinary';
import { CloudinaryService } from 'src/cloudinary/cloudinary.service';
import { JwtGuard } from 'src/auth/guards/jwt.guard';
import { FileInterceptor } from '@nestjs/platform-express';

@Controller('image-upload')
export class ImageUploadController {
  constructor(private cloudinary: CloudinaryService) {}

  @UseGuards(JwtGuard)
    @Post()
    @UseInterceptors(FileInterceptor('file'))
  async uploadImage(
    @UploadedFile() file: Express.Multer.File,
  ): Promise<UploadApiResponse | UploadApiErrorResponse> {
    return this.cloudinary.uploadImage(file).catch((e) => {
      throw new BadRequestException(e.message);
    });
  }

  @UseGuards(JwtGuard)
  @Delete('delete/:id')
  async deleteImage(@Param('id') id: string): Promise<{ result: string }> {
    return this.cloudinary.deleteImage(id);
  }
}

