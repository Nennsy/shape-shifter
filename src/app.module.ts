import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';

import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UsersModule } from './users/users.module';
import { User, UserSchema } from './users/schema/user.schema';
import { UsersService } from './users/users.service';
import { AuthModule } from './auth/auth.module';
import { AuthService } from './auth/auth.service';
import { JwtService } from '@nestjs/jwt';
import { ConfigModule } from '@nestjs/config';
import { MeasurementsModule } from './measurements/measurements.module';
import { CloudinaryModule } from './cloudinary/cloudinary.module';
import { ImageUploadService } from './image-upload/image-upload.service';
import { ImageUploadController } from './image-upload/image-upload.controller';
import { ImageUploadModule } from './image-upload/image-upload.module';
import { ExerciseTypeModule } from './exercise-type/exercise-type.module';
import { ExerciseModule } from './exercise/exercise.module';


@Module({
  imports: [
    ConfigModule.forRoot(),
    UsersModule,
    MeasurementsModule,
    MongooseModule.forRoot(process.env.DB_CONNECTION),
    MongooseModule.forFeature([{ name: User.name, schema: UserSchema }]),
    AuthModule,
    CloudinaryModule,
    ImageUploadModule,
    ExerciseTypeModule,
    ExerciseModule,
  ],
  controllers: [AppController, ImageUploadController],
  providers: [AppService, UsersService, AuthService, JwtService, ImageUploadService],
})
export class AppModule {}