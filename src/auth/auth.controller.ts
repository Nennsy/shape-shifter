import { Body, Controller, Post, HttpCode, HttpStatus } from '@nestjs/common';
import { CreatingUser } from 'src/users/dto/creating-user.dto';
import { ExistingUser } from 'src/users/dto/existing-user.dto';
import { UserDetails } from 'src/users/interfaces/user.interface';

import { AuthService } from './auth.service';

@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Post('register')
  register(@Body() user: CreatingUser): Promise<UserDetails | null> {
    return this.authService.register(user);
  }

  @Post('login')
  @HttpCode(HttpStatus.OK)
  login(@Body() user: ExistingUser): Promise<{ token: string } | null> {
    return this.authService.login(user);
  }
}
