import { HttpStatus, Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import * as bcrypt from 'bcrypt';
import { CreatingUser } from 'src/users/dto/creating-user.dto';
import { ExistingUser } from 'src/users/dto/existing-user.dto';
import { UserDetails } from 'src/users/interfaces/user.interface';
import { UsersService } from 'src/users/users.service';
import { HttpException } from '@nestjs/common';
import { hashPassword } from 'src/utils/helpers';


@Injectable()
export class AuthService {
  constructor(
    private userService: UsersService,
    private jwtService: JwtService,
  ) {}

  async hashPassword(password: string): Promise<string> {
    return bcrypt.hash(password, 12);
  }

  async register(user: CreatingUser): Promise<UserDetails | any> {
    const existingUser = await this.userService.findByEmail(user.email);
    if (existingUser)
      throw new HttpException('This email is taken!', HttpStatus.BAD_REQUEST);
    user.password = await hashPassword(user.password);
    const newUser = await this.userService.create(user);
    return this.userService._getUserDetails(newUser);
  }

  async doesThePasswordMatch(
    password: string,
    hashedPassword: string,
  ): Promise<boolean> {
    return bcrypt.compare(password, hashedPassword);
  }

  async validateUser(
    email: string,
    password: string,
  ): Promise<UserDetails | null> {
    const user = await this.userService.findByEmail(email);
    const doesUserExist = !!user;

    if (!doesUserExist) return null;

    const doesPasswordMatch = await this.doesThePasswordMatch(
      password,
      user.password,
    );
    if (!doesPasswordMatch) return null;

    return this.userService._getUserDetails(user);
  }

  async login(existingUser: ExistingUser): Promise<{ token: string } | null> {
    const { email, password } = existingUser;
    const user = await this.validateUser(email, password);

    if (!user)
      throw new HttpException(
        'Email and password do not match!',
        HttpStatus.BAD_REQUEST,
      );
    const jwt = await this.jwtService.signAsync({ user });
    return { token: jwt };
  }
}
