import { ExerciseTypeEnum } from "./exercise-type.enum"; 

export interface ExerciseTypeInterface {
    _id: string;
    name: string;
    muscleGroups: ExerciseTypeEnum[];
}
