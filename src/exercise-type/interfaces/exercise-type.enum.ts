export enum ExerciseTypeEnum {
    Deltoids = 'deltoids',
    Trapezius = 'trapezius',
    Biceps = 'biceps',
    Triceps = 'triceps',
    LatissimusDorsi = 'latissimus dorsi',
    Pectorals = 'pectorals',
    Abdominals = 'abdominals',
    Glutes = 'glutes',
    Quadriceps = 'quadriceps',
    Hamstrings = 'hamstrings',
    Calves = 'calves',
  }
  