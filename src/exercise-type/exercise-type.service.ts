import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CreateExerciseTypeDto } from './dto/create-et.dto';
import { UpdateExerciseTypeDto } from './dto/update-et.dto';
import { ExerciseTypeInterface } from './interfaces/exercise-type.interface';
import { ExerciseType, ExerciseTypeDocument } from './schemas/exercise-type.schema';

@Injectable()
export class ExerciseTypeService {

    constructor(
        @InjectModel(ExerciseType.name)
        private exerciseTypeModel: Model<ExerciseTypeDocument>,
    ) { }


    async createExerciseType(
        createExerciseTypeDto: CreateExerciseTypeDto,
    ): Promise<ExerciseTypeInterface> {
        const newExercise = new this.exerciseTypeModel({
            ...createExerciseTypeDto,
        });
        return newExercise.save();
    }

    async findAll(): Promise<ExerciseTypeInterface[]> {
        return this.exerciseTypeModel.find().lean();
    }

    async findOne(id: string): Promise<ExerciseTypeInterface> | null {
        const exercise = await this.exerciseTypeModel.findById(id).lean();
        if (!exercise) return null;
        return exercise;
    }

    async deleteExerciseType(id: string): Promise<ExerciseTypeInterface> | null {
        const existingExercise = await this.findOne(id);
        if (!existingExercise) {
          return null;
        } else {
          await this.exerciseTypeModel.deleteOne({ _id: id });
          return existingExercise;
        }
      }

  async updateExerciseType(
    id: string,
    updateExercise: UpdateExerciseTypeDto,
  ): Promise<ExerciseTypeInterface> | null {
   const existingExerciseType = await this.exerciseTypeModel.findById({ _id: id,});
   if(!existingExerciseType){
    return null;
   }else {
    return await this.exerciseTypeModel.findByIdAndUpdate(id, updateExercise,);
   }
  }



}
