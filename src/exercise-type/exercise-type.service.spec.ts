import { Test, TestingModule } from '@nestjs/testing';
import { ExerciseTypeService } from './exercise-type.service';

describe('ExerciseTypeService', () => {
  let service: ExerciseTypeService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ExerciseTypeService],
    }).compile();

    service = module.get<ExerciseTypeService>(ExerciseTypeService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
