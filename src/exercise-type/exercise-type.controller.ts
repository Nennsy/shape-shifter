import {
    Controller,
    Get,
    Post,
    Body,
    Patch,
    Param,
    Delete,
    Res,
    HttpStatus,
    UseGuards,
} from '@nestjs/common';
import { JwtGuard } from '../auth/guards/jwt.guard';
import { ExerciseTypeService } from './exercise-type.service';
import { RolesGuard } from './roles/roles.guard';
import { Roles } from './roles/roles-decorator';
import { Role } from '../users/interfaces/role.enum';
import { CreateExerciseTypeDto } from './dto/create-et.dto';
import { ExerciseTypeInterface } from './interfaces/exercise-type.interface';
import { UpdateExerciseTypeDto } from './dto/update-et.dto';

@Controller('exercise-type')
export class ExerciseTypeController {

    constructor(private exerciseTypeService: ExerciseTypeService) { }

    @Post()
    @UseGuards(JwtGuard, RolesGuard)
    @Roles(Role.Admin)
    async create(
      @Body() createExerciseTypeDto: CreateExerciseTypeDto,
    ): Promise<ExerciseTypeInterface> {
      const newExercise = await this.exerciseTypeService.createExerciseType(
        createExerciseTypeDto,
      );
      return newExercise;
    }

    @Get()
    async findAll(@Res() response): Promise<ExerciseTypeInterface[]> {
      const exercises = await this.exerciseTypeService.findAll();
      return response.status(HttpStatus.OK).json([...exercises]);
    }

    @Get(':id')
    async findOne(
      @Res() response,
      @Param('id') id: string,
    ): Promise<ExerciseTypeInterface> {
      const exercise = await this.exerciseTypeService.findOne(id);
      if (exercise) {
        return response.status(HttpStatus.OK).json({
          exercise,
        });
      } else {
        return response
          .status(HttpStatus.NOT_FOUND)
          .json({ message: 'Exercise type with id does not exist!' });
      }
    }

    @Delete(':id')
    @UseGuards(JwtGuard, RolesGuard)
    @Roles(Role.Admin)
    async remove(
      @Res() response,
      @Param('id') id: string,
    ): Promise<ExerciseTypeInterface> {
      const existingExercise = await this.exerciseTypeService.deleteExerciseType(id);
      if (existingExercise !== null) {
        return response.status(HttpStatus.NO_CONTENT).json({});
      } else {
        return response.status(HttpStatus.NOT_FOUND).json({
          message: 'User with id does not exist',
        });
      }
    }

    @Patch(':id')
    @UseGuards(JwtGuard, RolesGuard)
    @Roles(Role.Admin)
    async update(
      @Res() response,
      @Param('id') id: string,
      @Body() updateExerciseTypeDto: UpdateExerciseTypeDto,
    ): Promise<ExerciseTypeInterface> {
      const newExercise = await this.exerciseTypeService.updateExerciseType(
        id,
        updateExerciseTypeDto,
      );
      if (!newExercise) {
        return response
          .status(HttpStatus.NOT_FOUND)
          .json({ message: 'Exercise type with id does not exist' });
      } else {
        return response.status(HttpStatus.OK).json({
          ...newExercise,
        });
      }
    }

}
