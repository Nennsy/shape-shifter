import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import { ExerciseTypeEnum } from '../interfaces/exercise-type.enum';

export type ExerciseTypeDocument = ExerciseType & Document;

@Schema()
export class ExerciseType {
    @Prop({ required: true })
    name: string;

    @Prop({ required: true, type: [String], enum: ExerciseTypeEnum })
    muscleGroups: Array<ExerciseTypeEnum>;
}

export const ExerciseTypeSchema = SchemaFactory.createForClass(ExerciseType);
