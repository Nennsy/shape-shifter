import { IsArray, IsString } from 'class-validator';
import { ExerciseTypeEnum } from '../interfaces/exercise-type.enum';

export class CreateExerciseTypeDto {
    @IsString()
    name: string;
    @IsArray()
    muscleGroups: ExerciseTypeEnum[];
}
