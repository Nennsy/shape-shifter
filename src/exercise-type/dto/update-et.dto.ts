import { IsArray, IsOptional, IsString } from 'class-validator';
import { ExerciseTypeEnum } from '../interfaces/exercise-type.enum';

export class UpdateExerciseTypeDto {
    @IsOptional()
    @IsString()
    name: string;
    @IsOptional()
    @IsArray()
    muscleGroups: ExerciseTypeEnum[];
}
