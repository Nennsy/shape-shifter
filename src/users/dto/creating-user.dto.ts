import { Role } from '../interfaces/role.enum';

export class CreatingUser {
  name: string;
  email: string;
  password: string;
  role: Role;
  gender: string;
  dateOfBirth: string;
  height: number;
}
