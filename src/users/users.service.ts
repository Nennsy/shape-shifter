import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import mongoose, { Model } from 'mongoose';
import { UserDetails } from './interfaces/user.interface';
import { User, UserDocument } from './schema/user.schema';
import { CreatingUser } from './dto/creating-user.dto';
import { AuthService } from 'src/auth/auth.service';
import { hashPassword } from 'src/utils/helpers';

@Injectable()
export class UsersService {
  constructor(@InjectModel(User.name) private userModel: Model<UserDocument>) { }

  _getUserDetails(user: UserDocument): UserDetails {
    return {
      id: user._id,
      name: user.name,
      email: user.email,
      password: user.password,
      role: user.role,
      gender: user.gender,
      dateOfBirth: user.dateOfBirth,
      height: user.height,
    };
  }

  async findByEmail(email: string): Promise<UserDocument | null> {
    return this.userModel.findOne({ email }).exec();
  }

  async findById(id: string): Promise<UserDocument | null> {
    const user = await this.userModel.findById(id).exec();
    if (!user) return null;
    return user;
  }

  async getAll(): Promise<User[]> {
    return await this.userModel.find().exec();
  }

  async getById(id: string) {
    return await this.userModel.findById(new mongoose.Types.ObjectId(id));
  }

  async create(user: CreatingUser): Promise<UserDocument> {
    const newUser = new this.userModel(user);
    return newUser.save();
  }

  async update(id: string, user: User): Promise<User> | null {
    const existingUser = await this.getById(id);
    if (!existingUser) {
      return null;
    } else {
      user.password = await hashPassword(user.password);
      return await this.userModel.findByIdAndUpdate(id, user, { new: true });
    }
  }

  async delete(id: string): Promise<User> | null {
    const existingUser = this.getById(id);
    if (!existingUser) {
      return null;
    } else {
      await this.userModel.findByIdAndRemove(new mongoose.Types.ObjectId(id));
      return existingUser;
    }
  }
}
