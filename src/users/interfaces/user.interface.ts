import { Role } from './role.enum';

export interface UserDetails {
  id: string;
  name: string;
  email: string;
  password: string;
  role: Role;
  gender: string;
  dateOfBirth: string;
  height: number;
}
