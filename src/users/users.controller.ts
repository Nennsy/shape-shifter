import {
  Body,
  Controller,
  Delete,
  Get,
  HttpStatus,
  Param,
  Patch,
  Post,
  Put,
  Res,
  UseGuards,
} from '@nestjs/common';
import { User, UserDocument } from './schema/user.schema';
import { UsersService } from './users.service';
import { JwtGuard } from 'src/auth/guards/jwt.guard';

@Controller('users')
export class UsersController {
  constructor(private usersService: UsersService) {}

  @Get()
  async getAll(@Res() response) {
    const users = await this.usersService.getAll();
    return response.status(HttpStatus.OK).json({
      users,
    });
  }
  @UseGuards(JwtGuard)
  @Get('/:id')
  async findById(@Res() response, @Param('id') id) {
    const user = await this.usersService.getById(id);
    return response.status(HttpStatus.OK).json({
      user,
    });
  }

  @Post()
  async addUser(@Res() response, @Body() user: User) {
    const newUser = await this.usersService.create(user);
    return response.status(HttpStatus.CREATED).json(newUser.toJSON());
  }

  @Patch(':id')
  async updateUser(
    @Res() response,
    @Param('id') id,
    @Body() user: User,
  ): Promise<User> {
    const updatedUser = await this.usersService.update(id, user);
    if (updatedUser) {
      return response.status(HttpStatus.OK).json(updatedUser);
    } else {
      return response.status(HttpStatus.NOT_FOUND).json({});
    }
  }

  @Delete(':id')
  async deleteUser(@Res() response, @Param('id') id): Promise<User> {
    const deletedUser = await this.usersService.delete(id);

    if (deletedUser !== null) {
      return response.status(204).json({
        deletedUser,
      });
    } else {
      return response.status(404).json({
        message: 'User with id does not exist',
      });
    }
  }
}
